# Bouncer

Just numbers.

![Numbers](bouncer-plaque.svg)


## Design Notes

Rectangular pixel in 8:9 aspect ratio (448 wide by 504 high).

Cap height 2520.

5 pixel high structure.

Within which is laid a second realisation of each glyph using
a Pinball-like circular hole.
The hole is 200 across.

# END
